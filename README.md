# The Trinity

Resources for the book _The Trinity: A Contemporary Update to the Classic Study of Biblical Trinitarianism_ by Edward Henry Bickersteth (originally published as _The Rock of Ages; or, Scripture Testimony to the One Eternal Godhead of the Father, and of the Son, and of the Holy Ghost_).

## License

Copyright © 2022 Steven Soloff

Permission is granted to copy, distribute and/or modify this document under the terms of the GNU Free Documentation License, Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts. A copy of the license is available [here](https://www.gnu.org/licenses/fdl.html).
