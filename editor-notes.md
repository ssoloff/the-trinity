# Editor's Notes

## Philosophy

Listed below is the philosophy the editor has employed in order to produce a more contemporary update of Bickersteth's original manuscript.

* Convert Scripture references to the New King James Version.
* Capitalize pronouns referring to God in order to be consistent with the NKJV.
* Replace the name of God, Jehovah, with Yahweh.
* Update archaic words (e.g., thee, thou, saith), "false friends," and words that have significantly fallen out of usage to equivalent words in more contemporary usage.
* Update archaic pronunciation (e.g., uses of colons and em dashes that no longer apply in contemporary grammar).
* Commas seem to be used much more liberally in 19th-century English, so some have been removed.  Likewise, commas have been added if it would help the reader understand a sentence without having to parse it a second or third time by introducing appropriate pauses that make the sentence more comprehensible.
* In general, I have not tried to change the author's sentence structure much because he has a passion that carries through his writing that I was afraid would be lost if I changed it too much.  However, some sentences have been paraphrased if they had an unclear meaning, but that meaning was mostly obvious after analyzing it further.  In some cases, the author's original intent was not clear at all, and so I left the sentence as is (other than vocabulary and punctuation changes); so if you are confused as to what a sentence means, so was I. :-)

Grammar style rules:

* Ellipses
  * Omission at end of grammatically-incomplete sentence: "word .<nbsp>.<nbsp>. Word"
  * Omission at end of grammatically-complete sentence: "word.<nbsp>.<nbsp>.<nbsp>. Word"
  * Omission before colon: "word.<nbsp>.<nbsp>.<nbsp>: word"
  * Omission before comma: "word.<nbsp>.<nbsp>.<nbsp>, word"
  * Omission before exclamation point: "word.<nbsp>.<nbsp>.<nbsp>! Word"
  * Omission before question mark: "word.<nbsp>.<nbsp>.<nbsp>? Word"
  * Omission before semicolon: "word.<nbsp>.<nbsp>.<nbsp>; word"
  * Omission after colon: "word: .<nbsp>.<nbsp>. [Ww]ord"
  * Omission after comma: "word, .<nbsp>.<nbsp>. word"
  * Omission after exclamation point: "word! .<nbsp>.<nbsp>. Word"
  * Omission after question mark: "word? .<nbsp>.<nbsp>. Word"
  * Omission after semicolon: "word; .<nbsp>.<nbsp>. word"

Citation/Reference style rules:

* Bible book abbreviations per CMOS.
* A space should appear after each comma in verse references (e.g., "Rom. 3:23, 26").
* A single space should appear between verse reference and version identifier for parenthetical references (e.g., "(Rom. 3:23 KJV)"). For inline references, the version identifier should appear in parentheses (e.g., "Romans 3:23 (KJV)").
* The Greek text associated with a Scripture quotation should be placed within square brackets (e.g., "One Lord [εἷς Κύριος]"). If a version identifier is required, it should appear in parentheses immediately before the closing square bracket (e.g., "One Lord [εἷς Κύριος (LXX)]").
* The English translation of a Greek word or phrase should appear after a comma following the Greek and be quoted (e.g., "Lord [Κύριος, "master"]").
* Annotations for Scripture references should appear after a semi-colon following the reference (e.g., "Rom. 3:23; see KJV margin note").
* Annotations for noting emphasis should be either "emphasis added" or "emphasis in original."

## Edits

### Changed words and phrases

The following are words or phrases originally used by Bickersteth that might not be considered contemporary. Following each entry is the more contemporary form chosen to be used in the manuscript. In some cases, the entry remains unchanged; it is listed below as documentation in order to avoid further research in the event it is encountered multiple times in the manuscript.

* abode ==> home
* abounding ==> abundant [except in Scriptures]
* absorbing ==> captivating
* acceptation ==> acceptance
* accountableness ==> accountability
* actings ==> actions
* adduce ==> cite, give, offer, quote
* adorable ==> worthy of adoration
* Administrator ==> Ruler
* adumbration ==> suggestion
* affect [verb in the sense of "pretend"] ==> feign
* affectation ==> pretense
* affiance ==> trust
* afford [in the sense of "give"] ==> provide
* afford [in the sense of "make available"] ==> make available
* after [adj, in the sense of "following in position or time"] ==> subsequent
* after all [in the sense of "finally"] ==> ultimately
* aged ==> elderly
* Agent [when not used as a title for God (see below)] ==> agent
* alacrity ==> enthusiasm
* alas ==> unfortunately
* all-might ==> omnipotence
* all-wisdom ==> omniscience
* allied ==> related
* Almighty [adj] ==> almighty
* Alone [adj] ==> alone
* alone [adj] ==> only
* aloud ==> with a loud voice
* amongst ==> among
* amplitude ==> breadth
* amply ==> abundantly
* an One ==> a One
* animalcule ==> microscopic organism
* any thing ==> anything
* Apocalypse ==> Revelation
* apology [in the sense of "defense"] ==> defense
* appellation ==> title
* appellative ==> title
* appertain ==> apply
* appreciate [in the sense of "recognize worth"] ==> understand
* apprehend [in the sense of "understand"] ==> believe
* ardent ==> enthusiastic
* arrogating ==> usurping
* as a ==> like a
* assail ==> attack
* assessor ==> one who sits at the right hand of
* asseverate ==> state categorically
* assiduity ==> diligence
* asunder ==> separate
* at all events ==> in any case
* aught ==> anything
* Author [noun] ==> author
* avers ==> affirms
* awakening ==> rousing
* awful [in the sense of "inspiring awe"] ==> awesome, holy
* baffled ==> confused
* Baptism ==> baptism
* base [adj] ==> despicable
* battle-field ==> battlefield
* be stumbled ==> stumble
* bearing (this) in mind ==> keeping (this) in mind
* bearing on this ==> pertaining to this
* behove ==> obligate
* beneficence ==> benevolence
* benighted ==> unenlightened
* besought ==> asked, begged, implored
* bestow ==> grant
* betake ==> commit
* betwixt ==> between
* bless [in the sense of "approve"] ==> approve
* [I] blush ==> [I am] embarrassed
* bound down ==> constrained
* breasts ==> hearts
* Bride ==> bride of Christ
* bring forward [in the sense of "introduce"] ==> present
* bring in [in the sense of "judgment"] ==> declare
* bulwark ==> defense
* calamity ==> tragedy
* calm [in the sense of "composed"] ==> impassive
* candid ==> honest
* cast them in his teeth ==> revile him with them
* catena ==> chain
* certified [in the sense of "certain"] ==> certain
* chamber ==> room
* chance ==> happen
* chancellor ==> prime minister
* character [in the sense of "portrayal of another"] ==> role
* chasten ==> subdue [except in Scriptures]
* (the) chart of providence ==> God's sovereign plan
* cherubic ==> angelic
* church of England ==> Church of England
* class ==> category
* cleave ==> cling
* cloven ==> divided
* co-equal ==> coequal
* co-essential ==> coessential
* co-eternal ==> coeternal
* co-extensive ==> coextensive
* co-operating ==> cooperating
* co-operation ==> cooperation
* co-ordinately ==> correspondingly, equally
* coalition ==> conjunction
* collated ==> examined
* collation ==> comparison, examination
* coincidence [in the sense of "agreement"] ==> agreement
* collateral [adj] ==> indirect
* combat [verb] ==> oppose
* Comforter ==> Helper
* commenced ==> begun
* compass [noun] ==> space
* conceive [in the sense of "creation"] ==> produce
* conceive [in the sense of "mental creation"] ==> think
* concentrates ==> focuses, gathers
* conception ==> understanding
* concur ==> agree
* confiding ==> entrusting, trusting
* confound ==> confuse
* congruous ==> suitable
* consentient ==> unanimous
* constrained [in the sense of "forced"] ==> forced
* constrained [in the sense of "limited"] ==> limited
* controvert ==> challenge
* convinces ==> convicts
* cordially ==> warmly
* countervail ==> nullify
* couplet ==> pair
* creature confidence ==> creaturely pride and self-confidence
* creature-eminence ==> creaturely eminence
* dedication stone ==> cornerstone
* defectible ==> prone to failure
* Deity [when not used as a title for God (see below)] ==> deity
* deluge ==> flood
* delusive ==> delusional
* demerit ==> offense
* denounce ==> proclaim
* deprecate ==> avoid, reject
* derange ==> disturbs
* design [noun in the sense of "intention"] ==> intention
* designed ==> intended, meant
* designs ==> intends
* despair [verb] ==> give up hope
* dictum ==> saying
* diffidence ==> hesitation
* dignities ==> noble qualities
* dignity ==> stature
* dimly apprehend ==> slowly understand
* discover ==> uncover
* discriminating sagacity ==> shrewd discernment
* display ==> ostentatiousness
* distils ==> is drawn out, materializes
* divers ==> diverse
* Divine [adj, when not used as a title for God (see below)] ==> divine
* Divine Revelation ==> divine revelation
* docile ==> easily taught
* doubt not ==> do not doubt
* draw [in the sense of "move something by pulling"] ==> gather
* draw near ==> approach
* drawn [in the sense of "deduce"] ==> made
* drawn out ==> deduced
* drift ==> progression
* duly ==> properly
* earnest ==> sincere
* efficacy ==> power
* efficiency ==> effectiveness
* effulgence ==> radiance
* effused ==> dispensed
* embassy [in the sense of "embassage"] ==> commission entrusted to
* emblematical ==> emblematic
* embraces [in the sense of "include in one's beliefs"] ==> includes
* eminence [in the sens of "high ground"] ==> elevation
* eminent ==> notable
* eminently ==> (most) notably
* Emmanuel ==> Immanuel
* enforce [in the sense of "implement"] ==> apply
* enforce [in the sense of "reinforce"] ==> reinforce
* engrafted ==> inspired
* engrave ==> deeply impress
* engross ==> captivate
* enjoin ==> command
* enlargement ==> expansion
* enlightment ==> enlightenment
* enrich ==> improve
* entanglement ==> complication
* enter into ==> consider [except in Scriptures]
* entire ==> complete
* entirely ==> completely
* entreat ==> ask
* entreaty ==> plea
* entwine ==> knit
* enunciated ==> spoken
* Epistle ==> epistle [when not referring to a canonical book or division; e.g., "Peter's first epistle"]
* equipoise ==> equilibrium
* ere ==> before
* erect ==> offer
* especially ==> particularly, specifically
* essay [in the sense of "book"] ==> book
* essay [in the sense of "brief paper"] ==> article, pamphlet
* estate [in the sense of "class, rank"] ==> condition
* Eternal [adj] ==> eternal
* (the) Eternal [noun] ==> (the) eternal One
* ever ==> always
* ever more ==> forever
* evince ==> attest
* exceeding great ==> exceedingly great
* excellence ==> virtue
* excellences ==> virtues
* excite ==> stir up
* exemplar ==> ideal model
* expostulate ==> argue
* expostulation ==> rebuke
* express [adj] ==> explicit, specific [except in Scriptures]
* fain [adv] ==> gladly
* fair [in the sense of "not foul"] ==> good
* fairly [in the sense of "justly"] ==> reasonably
* fall low ==> fall down
* fastened ==> fixed
* Fatherly ==> fatherly
* fathom ==> understand
* fathomed ==> unraveled
* feat ==> act
* feels [in the sense of "believe"] ==> believes
* felicity ==> happiness
* fellow ==> neighbor, peer
* fellow-men ==> fellow men
* fellow-sufferer ==> fellow sufferer
* fenced with ==> guarded by
* fiat ==> proclamation
* figure [in the sense of "figure of speech"] ==> expression
* firmament (of heaven) [except when speaking of the heavenly throne room (see below)] ==> sky
* first Article ==> first article
* first-born ==> firstborn
* fitted ==> suited
* fittest ==> most suitable
* fix upon ==> choose
* flit ==> move rapidly
* for ever ==> forever
* for ever and for ever ==> forever and ever
* forbear ==> refrain
* fore-knowledge ==> foreknowledge
* formulary ==> formula
* foster ==> cultivate
* fostering ==> cultivating
* founded ==> based
* fountain ==> source [except when speaking of Christ as the "fountain of ..."]
* fretting ==> raging
* from time to time ==> occasionally
* functionary [adj] ==> functional
* further ==> furthermore
* futurity ==> the future
* gainsay ==> disavow
* gayety ==> merriment
* germ ==> seed
* God-head ==> Godhead
* God-like ==> godlike
* god-like ==> godlike
* God Jehovah ==> Yahweh God
* Godhood ==> Godhead
* Godlike ==> godlike
* goodliness ==> lovelinesss
* goodly ==> sizable
* Gospel [adj] ==> gospel
* Governor ==> Ruler
* grand-children ==> grandchildren
* grateful obligations ==> gratitude
* guardedly ==> carefully
* hamper ==> break
* haply ==> perhaps
* hasten ==> move quickly
* hateful ==> vile
* Head ==> head
* heart-experience ==> emotional experience
* heart-knowledge ==> heart knowledge
* heed ==> attention
* hence ==> therefore
* hereafter ==> after this
* hereby ==> by this means
* herein ==> in this
* hiding-place ==> hiding place
* his Majesty [in reference to a monarch] ==> His Majesty
* Holy Ghost ==> Holy Spirit
* Holy Writ ==> Holy Scripture
* householder ==> head of household
* human kind ==> humanity
* identified [in the sense of "united"] ==> united
* ill-affected ==> not having a good disposition
* illimitable ==> unlimited
* imminent ==> likely
* immortality of weal or woe ==> issue of eternal life or eternal death
* Immutable [adj] ==> immutable
* import ==> importance
* imports ==> expresses
* importunity ==> urgent persistence
* in consequence ==> consequently
* in heaven and earth ==> in heaven and on earth
* in like manner ==> likewise [except in Scriptures]
* in respect of ==> with respect to
* in the main [in the sense of "substantially"] ==> substantially
* in thought ==> in retrospect
* in truth ==> indeed [except in Scriptures]
* in virtue ==> by virtue
* incessant ==> unrelenting
* incident ==> occurrence
* Incomprehensible [adj] ==> incomprehensible
* increate ==> uncreated
* incredulity ==> disbelief
* incrusted ==> encrusted
* inculcated ==> frequently taught
* indefectible ==> faultless
* indissolubly ==> inseparably, permanently
* induce ==> persuade
* ineffaceable ==> indelible
* inestimable ==> priceless
* Infinite [adj] ==> infinite
* Infinitude ==> infinitude
* inimitable ==> matchless
* inmost ==> innermost
* insuperable ==> insurmountable
* insure ==> ensure
* intelligences [in the sense of "beings"] ==> creatures
* intercourse [in the sense of "communication"] ==> communication
* intermission ==> interruption
* interpenetrates ==> permeates
* into the lips ==> on the lips
* Invisible [adj] ==> invisible
* irradiates ==> illuminates
* irrefragable ==> irrefutable
* Jehovah ==> Yahweh
* judicious ==> wise
* justly ==> rightly
* keep back ==> hold back
* kinsmen ==> countrymen
* laboured syllogisms ==> convoluted arguments
* lapse [in the sense of "break in action"] ==> passage
* lately ==> recently
* [the] less of ==> less of
* lest ==> for fear that [except in Scriptures]
* lie [in the sense of "be situated"] ==> exist
* life-boat ==> lifeboat
* light [noun in the sense of "understanding"] ==> understanding
* light upon ==> befall
* limpet ==> mollusk
* lively ==> vigorous
* lively oracles ==> living oracles
* Lord's prayer ==> Lord's Prayer
* lustrous ==> radiant
* made acquainted with ==> apprised of
* Majesty [in reference to an attribute, not a title] ==> majesty
* make the trial ==> try it for yourself
* manifest [adj] ==> apparent, obvious
* manifold ==> many
* mariner ==> sailor
* mark ==> note
* marked ==> notable
* marks [in the sense of "characterize"] ==> characterizes
* marks [in the sense of "notice"] ==> notes
* mart ==> market
* mazy ==> twisting
* met with ==> encountered
* metropolitan ==> worldly
* might well ==> could
* moment [in the sense of "importance"] ==> importance
* momentous ==> important
* more rich ==> richer
* multiplied ==> numerous
* must needs [adv] ==> must necessarily
* Name ==> name
* nay ==> no
* nay more ==> in fact
* nay verily ==> no indeed
* needful ==> necessary
* needs ==> requires
* negative immateriality ==> incorporeity
* negatived ==> refuted
* nerved ==> strengthened
* new-born ==> newborn
* new-creates ==> newly creates
* nigh ==> close
* Noëtus ==> Noetus
* non-exertion ==> inexertion
* non-existence ==> nonexistence
* O ==> Oh [except in Scriptures]
* Omnipotence [noun, as a title for God] ==> the omnipotent One
* Omnipotent [adj] ==> omnipotent
* Omnipresent [adj] ==> omnipresent
* Omniscience [noun, as a title for God] ==> the omniscient One
* Omniscient [adj] ==> omniscient
* One [adj] ==> one
* only-begotten ==> only begotten
* outflowing ==> outpouring
* pains-taking ==> painstaking
* panoply ==> full armor
* Paradise ==> paradise
* part [in the sense of "a person's interest"] ==> task
* pass on ==> continue
* passes ==> surpasses
* pathetic [in the sense of "sorrow"] ==> heartbreaking
* pathway ==> path
* peculiar ==> characteristic
* peculiar propriety ==> characteristic function
* peculiarities ==> distinguishing characteristics
* peculiarly conclusive ==> particularly conclusive
* perforce ==> by necessity
* peroration ==> conclusion
* Person ==> person
* Personal ==> personal
* Persons ==> persons
* perusal ==> examination
* pilot ==> captain
* pitiful ==> compassionate
* the plague [in the sense of "the fall of man"] ==> the fall of man
* plenipotentiary ==> diplomat
* plenitude ==> abundance
* plenteousness ==> abundance
* prattling childhood ==> carefree childhood
* pray [in the sense of "making a request" of the reader] ==> implore
* prayerful ==> sincere
* pre-eminence ==> preeminence
* pre-existent ==> preexistent
* precipice ==> sheer drop
* predicated ==> affirmed
* preponderating ==> overwhelming
* pretension ==> pretext
* prevalent ==> powerful
* probe [noun] ==> inquiry
* prodigies ==> extraordinary feats
* proffered ==> offered
* proof-sheets ==> advance copy
* prophetical ==> prophetic
* propriety ==> appropriateness
* proto-martyr ==> first martyr
* Psalmist ==> psalmist
* put forth ==> exert
* Pye Smith ==> Pye-Smith [John Pye-Smith per https://en.wikipedia.org/wiki/John_Pye-Smith]
* quell ==> suppress
* quelling ==> calming
* quickening ==> energizing, enlivening
* quickens ==> gives life to
* quickly succeeding ==> immediately succeeding
* quitting ==> abandoning
* rapine ==> pillage and plunder
* reading [in the sense of knowledge] ==> knowledge
* realize [in the sense of "appreciate"] ==> appreciate
* rebounding [assumed to be a misspelling of redounding] ==> overflowing
* received version ==> Received Text
* reconcilement ==> reconciliation
* rectified ==> corrected
* recur ==> come again to mind, return
* redound ==> overflow
* reflex [adj] ==> automatic
* regard [noun in the sense of "affection"] ==> affection
* regard [noun in the sense of "attention"] ==> attention
* regard [verb in the sense of "judge"] ==> consider
* remarking ==> noticing
* rent in twain ==> torn in two
* repose [in the sense of "relying upon"] ==> place
* requisite ==> required
* respect [verb in the sense of "concerning"] ==> refer to
* respect being had to ==> with respect to
* respecting ==> referring to
* rest on [verb in the sense of "depend"] ==> depend on, place in
* retaliate ==> dispute
* retire alone ==> seclude yourself
* (the) Rev. [i.e., "Reverend"] ==> Rev. [except in citations]
* Romanists ==> Roman Catholic Church
* round about ==> around and around
* ruled ==> judged
* Sacred [adj] ==> sacred
* sagacity ==> wisdom
* salutation ==> greeting
* salute ==> respectfully greet
* sameness ==> similarity
* satisfaction ==> comfort
* scarcely ==> barely
* Scriptural ==> scriptural
* scruple [verb] ==> hesitate
* second advent ==> second coming
* security [in the sense of "peace of mind"] ==> confidence
* self-abnegation ==> self-denial
* self-dependence ==> self-reliance
* Self-Existent ==> self-existent
* self-government ==> self-rule
* self-knowledge ==> self-understanding
* self-same ==> identical
* senate ==> government
* sense ==> appreciation
* seraph ==> angel
* seraphic ==> angelic
* set at naught ==> disdain
* shall ==> will [except in Scriptures]
* short-sightedness ==> shortsightedness
* sifting ==> analysis
* signification ==> meaning
* sketch ==> outline
* smite ==> strike
* so with regard ==> and so it is with regard
* solaces ==> consoles
* Son of man ==> Son of Man
* sore displeased ==> indignant
* space forbids ==> space prevents
* stablish ==> establish
* standing forth ==> presented as
* stands forth ==> stands
* stoops ==> condescends
* straightway ==> immediately
* strengthless ==> feeble
* stress ==> emphasis
* styled ==> designated
* styling ==> identifying
* suasion ==> exhortation
* suasive ==> persuasive
* subscribe [in the sense of "agree"] ==> agree
* succour ==> assistance
* suffer [in the sense of "permitting"] ==> permit
* suitor ==> petitioner
* sun-beam ==> ray of sunlight
* sunken ==> fallen
* supereminent ==> preeminent
* suppliant [adj] ==> supplicating
* suppliant [noun] ==> supplicant
* Supremacy ==> supremacy
* Supreme [adj] ==> supreme
* surety ==> guarantee
* suspecting ==> distrusting
* suspicion ==> doubt, skepticism
* sustain [in a very limited sense when used with "role"] ==> undertake
* sustentation ==> upholding
* tempest-tossed ==> storm-tossed, troubled
* temptor ==> devil
* tend ==> contribute
* tenet ==> doctrine
* thence ==> from that place
* thereby ==> by that
* therein ==> in that <thing>, in those <things>
* thereon ==> upon
* therewith ==> with that
* thou ==> you
* thraldom ==> bondage
* three-fold ==> threefold
* Throne ==> throne
* to my cost ==> to my detriment
* tongue ==> language [except in Scriptures]
* towards ==> in relation to [only in some cases]
* transparent ==> clear
* treading ==> climbing
* treating ==> expounding
* treatise [when referring to this manuscript] ==> book
* tremulous ==> timid
* trifling [verb] ==> toying with
* trishagion ==> Trisagion
* Triune [adj] ==> triune
* trodden ==> followed
* unanswerable ==> irrefutable
* unanswerably ==> irrefutably
* uncancelled ==> intact
* uncircumscribed ==> unbounded
* Uncreated [adj] ==> uncreated
* undesignedly ==> unintentionally
* unemployed ==> unused
* unexampled ==> unprecedented
* unexerted ==> unexercised
* unfathomable ==> incomprehensible
* uniformly ==> consistently
* Unity of God ==> unity of God
* unjustly [in the sense of "wrongly"] ==> wrongly
* unlearned ==> uneducated
* unmeaning ==> meaningless
* unperplexed ==> straightforward
* untiring ==> tireless
* unutterably ==> inexpressibly
* unwarranted ==> unjustified
* upbraid ==> reprimand
* upland ==> uphill
* urbanity ==> graciousness
* vantage ground ==> vantage point
* vent [noun] ==> outlet
* venture ==> attempt [not in all cases because "venture" has a specific connotation of "to offer at the risk of rebuff, rejection, or censure"]
* veriest pauper's field ==> densest pauper's graveyard
* verily ==> confidently
* verity ==> truth
* vice-regal ==> viceroy
* visit [verb] ==> impose [except in Scriptures]
* volume [in the sense of "book"] ==> book, work
* voluminous ==> large
* vouchsafed ==> graciously permitted
* wait [noun] ==> expectation
* want [noun in the sense "lack"] ==> lack
* wanting ==> lacking
* wards ==> locks, tumblers
* warrant [noun] ==> basis, permission
* warrant [verb] ==> affirm, justify
* wax ==> become
* weigh ==> consider
* weight [in the sense of "importance"] ==> importance
* well-disposed ==> having a good disposition
* whatever be ==> regardless of
* whence ==> from where
* whereas ==> although
* whereby ==> by which
* wherein ==> in which
* wherewith ==> by which
* whilst ==> while
* wilful ==> willful
* wilfully ==> willfully
* woe ==> suffering [except in Scriptures]
* wont ==> accustomed
* wreck ==> shipwreck
* wrought ==> worked
* yea ==> indeed
* yea more ==> indeed even more

### Retained words and phrases

The following are words or phrases that were considered to be changed during the editing process, but, for one reason or another, were left intact. They are listed here as documentation in order to avoid further research in the event they are encountered multiple times in the manuscript.

* Agent [in reference to God]
* allude
* almightiness
* (the) Almighty [noun, in reference to God]
* apposition
* ascribe
* ascription
* austerity
* awakened
* (the) Awarder [in reference to God]
* beautify
* beget
* Being [noun, in reference to God]
* Book [as an alternate title for the Bible]
* (the) Bridegroom [in reference to God]
* Brother [in reference to God]
* candor
* Cause [noun, in reference to God]
* (the) Chief Shepherd [in reference to God]
* clinch
* concede
* conjunction
* consequent
* cordial
* couched
* (the) Creator [in reference to God]
* deigned
* (the) Deity [in reference to God]
* (the) Divine Helper [in reference to God]
* (the) Divine Majesty [in reference to God]
* (the) Divine Savior [in reference to God]
* (the) Divine Spirit [in reference to God]
* (the) Divine Word [in reference to God]
* efficacious
* emancipating
* emblem
* epitome
* Essence [in reference to God, only when it is used in the same sense as Being]
* eucharistic
* exhibit
* expositor
* express image
* fatness
* filial
* firmament [in reference to the heavenly throne room described in Scripture]
* (the) First Cause [in reference to God]
* forbearance
* Founder [in reference to God]
* Fountain [in reference to God]
* gather
* gather up
* (the) Good One [in reference to God]
* Gospel [as part of or substitute for a title of a canonical work]
* Gospels [when referring to the division of the canon]
* grapple
* Guest [in reference to God]
* High Priest [in reference to God]
* (the) Highest [in reference to God]
* (the) Holy Deity
* (the) Holy One [in reference to God]
* Holy Trinity
* homage
* inalienable
* incipient
* interline
* irrepressible
* Jealous [only when used as a name for God]
* (the) Judge [in reference to God]
* learned
* Legislator [in reference to God]
* (the) Majesty [in reference to God]
* (the) Maker [in reference to God]
* (the) Mediator [in reference to God; but not in the general sense, e.g., "a mediator"]
* mediatorial
* metonymy
* (the) Most High [in reference to God]
* Most High God
* Most High Judge [in reference to God]
* munificence
* One [noun, in reference to God]
* penitent
* perplexity
* petition [in the sense of "prayer"]
* plaintive
* ponder
* precept
* prefigure
* (the) Preserver [in reference to God]
* Prophet [in reference to God]
* provocation
* rankle
* (the) Refuge [in reference to God]
* (the) Ruler [in reference to God]
* revealer
* Sanctifier
* sanction [verb]
* Scripture
* Scriptures
* (the) Searcher [in reference to God]
* semblance
* (the) Sent One [in reference to God]
* seraphim
* Shepherd [in reference to God]
* solemn
* sonship
* strewn
* subsistence
* supplicate
* surpassingly
* Three [noun, in reference to God]
* (the) Trinity [in reference to God]
* Trinity in Unity
* underived
* unhumbled
* (the) Unity [in reference to God]
* unremoved
* untenable
* Victim [in reference to God]
* wrung
* yet [except in the sense of "even"; e.g., "yet more" ==> "even more"]

## Decisions

This section documents potentially controversial editorial decisions.

* Preface, p. 6
  * "Though to many of these authors I have only been able to refer, as isolated passages led me to desire to know their judgment on contested interpretations." This sentence appears to refer to the fact that Bickersteth did not read the references he cited in their entirety but only consulted portions of them when trying to understand a debated passage of Scripture. It has been rewritten to more clearly indicate that understanding.
* Chapter 5, Subsection 4, p. 116
  * The quote "each in our degree and to the utmost bound of our finite capacity, even as God is full, with Divine goodness" is unattributed, and I can't find the original source, so I simply removed the quotes, thus attributing it to Bickersteth.
* Chapter 7, Subsection 1, p. 144, footnote (endnote 33)
  * The reference to Eccles. 12:1 in the quote from Pye-Smith does not appear in the digitized copy of his book that I have (see below). However, there is a footnote on p. 323 of that copy that indicates that scholarship no longer believes that Eccles. 12:1 contains a plural reference (however, my Accordance interlinear is showing that it is plural!). I am assuming Bickersteth had an earlier copy of Pye-Smith's work, and the Ecclesiastes passage was removed in a later edition because it no longer supported Pye-Smith's argument. Instead of simply removing that reference completely from the endnote, I placed it in square brackets to indicate an edit by Bickersteth.
* Chapter 7, Subsection 4, p. 163
  * The citation of Luke 17:36-38 should probably be to vv. 26-29. (Note that Luke 17 ends at v. 37, so the original reference doesn't even exist.)
* Chapter 7, Subsection 5, p. 170
  * The quote "the Divine mind [would have] stood in an immense solitariness" is from Pye-Smith, vol. 3, appendix 4. However, so are many other phrases in this paragraph. Because they are not quoted, I'm not going to quote this either because it could lead to confusion; plus, it's not an exact quote. The attached endnote explains that a lot of this content is derived from Pye-Smith and provides the citation. Leaving the quotation marks would also seem to require adding a citation at this point, but then the citation in the endnote becomes redundant.
* Chapter 7, Subsection 5, p. 175
  * The citation of Ezek. 1:28 doesn't seem to correspond to the referenced clause. I have chosen to leave it assuming Bickersteth knew what he was talking about.

## Bibliography

The following are the works cited by Bickersteth.

* _Book of Common Prayer_ by the Church of England.
  * https://en.wikipedia.org/wiki/Book_of_Common_Prayer
  * "Articles of Religion": https://www.churchofengland.org/prayer-and-worship/worship-texts-and-resources/book-common-prayer/articles-religion (https://archive.ph/9FFgT)
  * "A Catechism": https://www.churchofengland.org/prayer-and-worship/worship-texts-and-resources/book-common-prayer/catechism (https://archive.is/SHhya)
  * "Collect for Trinity Sunday": https://www.churchofengland.org/prayer-and-worship/worship-texts-and-resources/book-common-prayer/collects-epistles-and-gospels-46 (https://archive.ph/rMLvo)
* _The Catholic Doctrine of a Trinity_ by William Jones.
  * https://en.wikipedia.org/wiki/William_Jones_of_Nayland
  * https://books.google.com/books/about/The_Catholic_Doctrine_of_a_Trinity.html?id=ZQM3AAAAMAAJ (https://archive.is/4dV9a)
* _Cautions for the Times_ edited by the Archbishop of Dublin.
  * https://books.google.com/books?id=1_oCAAAAQAAJ (https://archive.ph/xykDf)
* _Contr. Jud. Op._ (_Against the Jews_) by John Chrysostom.
  * https://en.wikipedia.org/wiki/Adversus_Judaeos
  * https://en.wikisource.org/wiki/Eight_Homilies_Against_the_Jews
  * Images of Greek manuscript: https://www.loc.gov/resource/amedmonastery.00279382055-ms/
  * Benedictine editor: https://en.wikipedia.org/wiki/Bernard_de_Montfaucon
  * Meaning of Latin abbreviation "op." (work): https://en.wikipedia.org/wiki/List_of_Latin_abbreviations
  * Meaning of Greek abbreviation "κ. τ. λ." (etc.): https://www.wordsense.eu/%CE%BA.%CF%84.%CE%BB./
* _Discourses on the Principal Points of the Socinian Controversy_ by Ralph Wardlaw.
  * https://en.wikipedia.org/wiki/Ralph_Wardlaw
  * https://archive.org/details/discoursesonprin00wardrich (https://archive.is/pfLbC)
  * https://cvlesalfabegues.com/search/discourses-on-the-principal-points-of-the-socinian-controversy-second-edition/ (https://archive.is/h5BUo)
* _The Doctrine of Divine Love_ by Ernst Sartorius.
  * https://de.wikipedia.org/wiki/Ernst_Sartorius
  * https://books.google.com/books?id=h9cGAAAAQAAJ (pp. 3-8)
* _The Enquirer_.
  * **NO REFERENCES FOUND.**
* _Gloria Patri_ by Thomas Sadler.
  * https://www.amazon.com/Gloria-patri-scripture-doctrine-Bickersteths-ebook/dp/B07H6BKT8C (https://archive.is/zFtWm)
  * https://pohetoraxuhiduko.rangelyautomuseum.com/gloria-patri-book-31616rd.php (https://archive.is/4v7pb)
* _Greek Testament Critical Exegetical Commentary_ by Henry Alford.
  * https://www.studylight.org/commentaries/eng/hac.html
  * https://www.studylight.org/commentaries/eng/hac/colossians-2.html (https://archive.ph/SVdAO)
* _Hints for an Improved Translation of the New Testament_ by James Scholefield.
  * https://en.wikipedia.org/wiki/James_Scholefield
  * https://archive.org/details/hintsforanimpro00schogoog (https://archive.is/L601A)
* _Horae Mosaicae_ by George Stanley Faber.
  * https://en.wikipedia.org/wiki/George_Stanley_Faber
  * Vol. 2: https://books.google.com/books?id=x3VAAAAAcAAJ
* _Horae Solitariae_ by Ambrose Serle.
  * https://en.wikipedia.org/wiki/Ambrose_Serle
  * https://openlibrary.org/books/OL14013766M/Horae_Solitariae (https://archive.is/ghfug)
* _The Life of the Rev. Thomas Scott_ by John Scott.
  * https://en.wikipedia.org/wiki/Thomas_Scott_(commentator)
  * https://archive.org/details/liferevthomassc00scotgoog
* _Museum of Science and Art_ (12 volumes) by Dionysius Lardner.
  * https://en.wikipedia.org/wiki/Dionysius_Lardner
  * Vol. 7: https://books.google.com/books?id=0rTpjEcRmKEC&source=gbs_book_other_versions
* _Notes on the New Testament_ by Albert Barnes.
  * https://en.wikipedia.org/wiki/Albert_Barnes_(theologian)
  * https://books.google.com/books/about/Notes_on_the_New_Testament_Explanatory_a.html?id=OmMFAAAAQAAJ (vol. 14, on Revelation, not available)
  * https://www.studylight.org/commentaries/eng/bnb.html
* _Ode on the Death of the Duke of Wellington_ by Alfred Lord Tennyson.
  * https://americanliterature.com/author/alfred-lord-tennyson/poem/ode-on-the-death-of-the-duke-of-wellington (https://archive.md/ajXbj)
* _Private Thoughts upon Religion and a Christian Life_ by William Beveridge.
  * https://en.wikipedia.org/wiki/William_Beveridge_(bishop)
  * https://books.google.com/books/about/Private_Thoughts_Upon_Religion_and_a_Chr.html?id=au1LvgAACAAJ
* _The Scripture Testimony to the Messiah_ by John Pye-Smith.
  * Vol. 1: https://books.google.com/books/about/The_Scripture_Testimony_to_the_Messiah.html?id=-cYUAAAAYAAJ
  * Vol. 2: https://www.google.com/books/edition/The_Scripture_testimony_to_the_Messiah_a/hlMEAAAAQAAJ
  * Vol. 3: https://books.google.com.au/books?id=bi-hqTgvurYC (https://archive.is/f55df)
* Sermons on Divine Love by Henry Alford.
  * **NO REFERENCES FOUND.**
* _The Supreme Godhead of Christ_ by William Robert Gordon.
  * https://quod.lib.umich.edu/m/moa/ajh1063.0001.001 (https://archive.is/q3L3T)
  * https://www.amazon.com/Supreme-Godhead-Christ-Cornerstone-Christianity/dp/1163461164 (https://archive.is/bQ3Lt)
* _Theology Explained and Defended_ (5 vols. but later as 4 vols.) by Timothy Dwight IV.
  * https://en.wikipedia.org/wiki/Timothy_Dwight_IV
  * NOTE: Bickersteth quotes the original five-volume work, but only the revised four-volume work is available.
  * Vol. 1 (of 4): https://books.google.com/books/about/Theology_Explained_and_Defended.html?id=92wPAAAAIAAJ
* _The Treasures of Wisdom_ by Rev. Thomas Rawson Birks.
  * https://en.wikipedia.org/wiki/Thomas_Rawson_Birks
  * https://ebook-new.com/gets/book.php?id=AyUCAAAAQAAJ&item=the-treasures-of-wisdom&data=laesferadigital.com
* _A Treatise on the Deity of Jesus Christ and on the Doctrine of the Trinity_ by Serjeant Sellon.
  * https://www.seecoalharbour.com/book/a-treatise-on-the-deity-of-jesus-christ-and-the-doctrine-of-the-trinity-edited-by-e-g-marsh/ (https://archive.md/hTzel)
  * https://www.euro-book.net/books/gPFhAAAAcAAJ/a-treatise-on-the-deity-of-jesus-christ-and-the-doctrine-of-the-trinity-edited-by-e-g-marsh/baker-john-sellon/unknown-publisher/129/1847/9876543210XXX/seec (https://archive.md/8WOZ5)
* _Unitarianism Confuted: A Series of Lectures Delivered in Christ Church, Liverpool_
  * https://archive.org/details/unitarianismconf00live (https://archive.is/qxCkt)
* _Vital Christianity: Essays and Discourses on the Religions of Man and the Religion of God_ by Alexandre Vinet.
  * https://en.wikipedia.org/wiki/Alexandre_Vinet
  * https://www.amazon.com/Vital-Christianity-Discourses-Religions-Religion/dp/1333162251 (https://archive.is/jAcCB)

## Cited persons

* Dr. Channing
  * I believe the "Dr. Channing" Bickersteth refers to is the notable Unitarian theologian William Ellery Channing.  Even though Channing died in 1842, and Bickersteth's book was published in 1860, Channing is *not* quoted replying to Bickersteth, but replying to Dr. Gordon.  Thus, it is possible that he is in view here.  The only other alternative is Ellery's nephew William Henry Channing, who was also a Unitarian theologian.  However, while Henry lived concurrent with Bickerstath, he does not seem to have held the title of "Doctor."
    * https://en.wikipedia.org/wiki/William_Henry_Channing
    * https://en.wikipedia.org/wiki/William_Ellery_Channing
* Dr. Griesbach
  * Misspelled in some places as "Grisbach."
  * Johann Jakob Griesbach
  * https://en.wikipedia.org/wiki/Johann_Jakob_Griesbach
* John Howard (Newgate Prison)
  * https://sites.google.com/site/richardwoodbury1777/newgate-bristol (https://archive.ph/5eUcl)
