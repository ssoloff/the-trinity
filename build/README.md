# Build

This folder contains the scripts used to run the build process.

## Release process

Use the following process to publish a new release:

1. Tag the appropriate commit (usually HEAD) on the `main` branch:
    ```
    $ git tag <version>
    ```
    where _version_ is the manuscript revision number from the copyright page of the manuscript (e.g., `v1543`). Do not include the manuscript modification time that appears after the revision number.
1. Push the new tag upstream:
    ```
    $ git push --tags
    ```
1. Wait for the GitLab pipeline triggered by the new tag to finish.
1. From the project [Releases](https://gitlab.com/ssoloff/the-trinity/-/releases) page, click the **New release** button.
1. Select the tag you created in step (1).
1. Add the following release asset link:
    * URL: **`https://gitlab.com/ssoloff/the-trinity/-/jobs/<job-id>/artifacts/raw/.build/the-trinity-<version>.pdf`**, where _job-id_ is the identifier of the `deploy` job contained in the pipeline from step (3), and _version_ is the value from step (1).
    * Link title: **Manuscript**.
1. Save the release by clicking the **Create release** button.
