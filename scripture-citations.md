# Scripture Citations

The following are the unique Scripture verses cited in the text (in Scripture order; one verse per line). Note that this list only includes citations for Scripture text that is reproduced in the manuscript; it does not include free citations where the Scripture text is not embedded with the citation.

* Gen. 1:1
* Gen. 1:2
* Gen. 1:3
* Gen. 1:26
* Gen. 2:17
* Gen. 2:20
* Gen. 6:3
* Gen. 8:21
* Gen. 17:1
* Gen. 18:25
* Gen. 20:13
* Gen. 27:34
* Gen. 32:26
* Gen. 32:30
* Gen. 48:15
* Gen. 48:16
* Exod. 3:6
* Exod. 3:10
* Exod. 20:2
* Exod. 23:21
* Exod. 33:11
* Exod. 33:14
* Exod. 33:19
* Exod. 33:20
* Exod. 34:6
* Exod. 34:7
* Exod. 34:14
* Lev. 17:11
* Num. 6:24
* Num. 6:25
* Num. 6:26
* Num. 6:27
* Num. 11:17
* Num. 14:11
* Deut. 5:7
* Deut. 5:26
* Deut. 6:4
* Deut. 6:5
* Deut. 10:20
* Deut. 18:15
* Deut. 28:58
* Deut. 29:29
* Deut. 30:20
* Deut. 31:8
* Deut. 32:4
* Deut. 32:12
* Deut. 33:27
* Judg. 6:24
* Judg. 13:22
* Job 4:19
* Job 8:9
* Job 11:7
* Job 14:4
* Job 23:6
* Job 25:4
* Job 26:14
* Job 33:4
* Job 33:12
* Job 33:13
* Job 33:14
* Job 34:32
* 1 Sam. 2:2
* 1 Sam. 3:9
* 1 Sam. 16:7
* 2 Sam. 23:2
* 2 Sam. 23:3
* 1 Kings 8:27
* 1 Kings 8:39
* 1 Kings 18:37
* Neh. 9:5
* Neh. 9:6
* Neh. 9:7
* Neh. 9:20
* Neh. 9:27
* Ps. 2:7
* Ps. 2:12
* Ps. 9:4
* Ps. 11:6
* Ps. 18:2
* Ps. 19:7
* Ps. 23:1
* Ps. 23:2
* Ps. 31:5
* Ps. 33:6
* Ps. 33:11
* Ps. 33:15
* Ps. 34:8
* Ps. 34:18
* Ps. 36:9
* Ps. 39:7
* Ps. 42:7
* Ps. 47:8
* Ps. 49:7
* Ps. 49:8
* Ps. 49:15
* Ps. 51:5
* Ps. 62:11
* Ps. 62:12
* Ps. 73:24
* Ps. 77:19
* Ps. 78:38
* Ps. 80:1
* Ps. 83:18
* Ps. 85:10
* Ps. 90:1
* Ps. 90:2
* Ps. 90:5
* Ps. 90:6
* Ps. 91:4
* Ps. 92:8
* Ps. 93:2
* Ps. 94:11
* Ps. 95:6
* Ps. 95:7
* Ps. 95:8
* Ps. 95:9
* Ps. 100:3
* Ps. 103:19
* Ps. 104:30
* Ps. 104:31
* Ps. 107:28
* Ps. 107:29
* Ps. 111:2
* Ps. 119:11
* Ps. 119:28
* Ps. 119:130
* Ps. 119:142
* Ps. 130:7
* Ps. 130:8
* Ps. 135:6
* Ps. 136:1
* Ps. 136:2
* Ps. 136:3
* Ps. 136:4
* Ps. 138:3
* Ps. 138:6
* Ps. 139:7
* Ps. 139:8
* Ps. 139:23
* Ps. 139:24
* Ps. 143:7
* Ps. 143:10
* Ps. 145:13
* Ps. 145:18
* Ps. 146:3
* Ps. 146:4
* Ps. 146:5
* Ps. 146:6
* Ps. 149:2
* Prov. 1:28
* Prov. 2:5
* Prov. 3:12
* Prov. 14:6
* Prov. 16:4
* Prov. 18:10
* Prov. 22:1
* Prov. 22:15
* Prov. 24:9
* Prov. 27:6
* Eccles. 3:11
* Eccles. 7:20
* Eccles. 12:1
* Eccles. 12:7
* Song of Sol. 8:6
* Isa. 1:2
* Isa. 2:22
* Isa. 6:3
* Isa. 6:5
* Isa. 6:8
* Isa. 7:14
* Isa. 8:13
* Isa. 8:14
* Isa. 9:6
* Isa. 11:1
* Isa. 11:2
* Isa. 11:3
* Isa. 11:4
* Isa. 24:15
* Isa. 25:4
* Isa. 25:8
* Isa. 26:4
* Isa. 27:8
* Isa. 31:1
* Isa. 31:3
* Isa. 31:6
* Isa. 32:2
* Isa. 40:3
* Isa. 40:6
* Isa. 40:7
* Isa. 40:8
* Isa. 40:10
* Isa. 40:11
* Isa. 40:12
* Isa. 40:13
* Isa. 40:14
* Isa. 40:18
* Isa. 42:8
* Isa. 43:3
* Isa. 43:11
* Isa. 43:25
* Isa. 44:6
* Isa. 44:24
* Isa. 44:25
* Isa. 45:5
* Isa. 45:9
* Isa. 45:12
* Isa. 45:15
* Isa. 45:19
* Isa. 45:21
* Isa. 45:22
* Isa. 45:23
* Isa. 45:24
* Isa. 45:25
* Isa. 46:9
* Isa. 48:16
* Isa. 49:26
* Isa. 50:10
* Isa. 51:12
* Isa. 51:13
* Isa. 53:3
* Isa. 53:5
* Isa. 53:6
* Isa. 53:7
* Isa. 53:10
* Isa. 53:12
* Isa. 54:5
* Isa. 57:15
* Isa. 61:1
* Isa. 61:2
* Isa. 61:10
* Isa. 63:1
* Isa. 63:10
* Isa. 63:11
* Isa. 63:12
* Isa. 63:13
* Isa. 63:14
* Isa. 63:15
* Isa. 63:16
* Isa. 64:6
* Isa. 66:13
* Jer. 10:10
* Jer. 13:23
* Jer. 17:5
* Jer. 17:6
* Jer. 17:7
* Jer. 17:8
* Jer. 17:9
* Jer. 17:10
* Jer. 23:5
* Jer. 23:6
* Jer. 23:24
* Jer. 29:11
* Jer. 29:12
* Jer. 29:13
* Jer. 31:25
* Lam. 3:22
* Ezek. 1:26
* Ezek. 2:4
* Ezek. 3:12
* Ezek. 8:1
* Ezek. 8:3
* Ezek. 20:11
* Ezek. 20:21
* Ezek. 34:15
* Ezek. 34:16
* Ezek. 37:9
* Ezek. 37:14
* Dan. 2:20
* Dan. 7:14
* Hosea 12:3
* Hosea 12:4
* Hosea 13:14
* Hosea 14:8
* Joel 2:28
* Mic. 4:5
* Mic. 5:2
* Hab. 1:13
* Hab. 2:2
* Zech. 3:9
* Zech. 4:10
* Zech. 12:1
* Zech. 12:10
* Zech. 14:9
* Mal. 1:6
* Mal. 3:6
* Matt. 1:21
* Matt. 3:3
* Matt. 3:7
* Matt. 3:17
* Matt. 4:6
* Matt. 4:7
* Matt. 4:10
* Matt. 5:22
* Matt. 5:28
* Matt. 5:48
* Matt. 6:9
* Matt. 6:10
* Matt. 6:11
* Matt. 6:12
* Matt. 6:13
* Matt. 7:23
* Matt. 8:2
* Matt. 8:25
* Matt. 8:26
* Matt. 9:18
* Matt. 9:27
* Matt. 9:38
* Matt. 10:20
* Matt. 10:28
* Matt. 11:27
* Matt. 11:28
* Matt. 11:29
* Matt. 12:21
* Matt. 12:32
* Matt. 14:33
* Matt. 15:25
* Matt. 16:16
* Matt. 17:5
* Matt. 18:20
* Matt. 19:16
* Matt. 19:17
* Matt. 19:26
* Matt. 20:23
* Matt. 21:9
* Matt. 21:16
* Matt. 21:44
* Matt. 23:8
* Matt. 23:10
* Matt. 23:37
* Matt. 25:31
* Matt. 25:32
* Matt. 25:41
* Matt. 26:38
* Matt. 28:18
* Matt. 28:19
* Matt. 28:20
* Mark 2:5
* Mark 7:21
* Mark 7:22
* Mark 7:23
* Mark 9:23
* Mark 13:32
* Mark 14:61
* Mark 16:16
* Luke 1:32
* Luke 1:33
* Luke 1:35
* Luke 1:47
* Luke 1:68
* Luke 1:69
* Luke 1:70
* Luke 1:76
* Luke 2:11
* Luke 2:14
* Luke 2:26
* Luke 3:21
* Luke 3:22
* Luke 4:8
* Luke 4:14
* Luke 10:16
* Luke 10:27
* Luke 11:13
* Luke 12:26
* Luke 13:25
* Luke 16:22
* Luke 16:23
* Luke 19:10
* Luke 19:40
* John 1:1
* John 1:2
* John 1:3
* John 1:4
* John 1:5
* John 1:6
* John 1:7
* John 1:8
* John 1:9
* John 1:10
* John 1:11
* John 1:12
* John 1:13
* John 1:14
* John 1:15
* John 1:18
* John 1:29
* John 2:19
* John 2:24
* John 2:25
* John 3:6
* John 3:7
* John 3:8
* John 3:13
* John 3:15
* John 3:16
* John 3:29
* John 3:35
* John 3:36
* John 4:10
* John 4:14
* John 4:24
* John 4:42
* John 5:17
* John 5:18
* John 5:19
* John 5:20
* John 5:21
* John 5:22
* John 5:23
* John 5:24
* John 5:25
* John 5:26
* John 5:27
* John 5:28
* John 5:29
* John 5:30
* John 5:40
* John 6:38
* John 6:40
* John 6:44
* John 6:68
* John 7:28
* John 7:37
* John 8:17
* John 8:18
* John 8:24
* John 8:29
* John 8:42
* John 9:35
* John 9:36
* John 10:3
* John 10:11
* John 10:14
* John 10:15
* John 10:16
* John 10:17
* John 10:18
* John 10:25
* John 10:27
* John 10:28
* John 10:30
* John 10:35
* John 11:41
* John 11:42
* John 12:32
* John 12:41
* John 14:1
* John 14:2
* John 14:3
* John 14:6
* John 14:9
* John 14:10
* John 14:12
* John 14:13
* John 14:15
* John 14:16
* John 14:17
* John 14:19
* John 14:21
* John 14:23
* John 14:26
* John 14:28
* John 15:4
* John 15:5
* John 15:9
* John 15:10
* John 15:15
* John 15:26
* John 15:27
* John 16:7
* John 16:12
* John 16:13
* John 16:14
* John 17:1
* John 17:3
* John 17:11
* John 17:22
* John 17:24
* John 17:25
* John 19:37
* John 20:28
* John 21:15
* John 21:16
* John 21:17
* Acts 2:4
* Acts 2:21
* Acts 2:22
* Acts 2:24
* Acts 2:33
* Acts 3:14
* Acts 3:16
* Acts 3:21
* Acts 4:12
* Acts 4:29
* Acts 4:30
* Acts 5:3
* Acts 5:4
* Acts 5:9
* Acts 5:32
* Acts 7:51
* Acts 7:55
* Acts 7:56
* Acts 7:57
* Acts 7:58
* Acts 7:59
* Acts 7:60
* Acts 8:29
* Acts 8:39
* Acts 9:14
* Acts 9:21
* Acts 9:31
* Acts 9:34
* Acts 10:19
* Acts 10:20
* Acts 10:36
* Acts 10:38
* Acts 10:42
* Acts 11:12
* Acts 13:2
* Acts 13:3
* Acts 13:4
* Acts 15:11
* Acts 15:18
* Acts 15:28
* Acts 16:6
* Acts 16:7
* Acts 16:31
* Acts 17:27
* Acts 17:28
* Acts 20:28
* Acts 21:14
* Acts 22:16
* Acts 28:25
* Rom. 1:4
* Rom. 1:7
* Rom. 1:18
* Rom. 2:4
* Rom. 2:5
* Rom. 2:6
* Rom. 2:7
* Rom. 2:8
* Rom. 2:9
* Rom. 3:10
* Rom. 3:19
* Rom. 3:20
* Rom. 3:21
* Rom. 3:22
* Rom. 3:23
* Rom. 3:24
* Rom. 3:25
* Rom. 3:26
* Rom. 4:25
* Rom. 5:5
* Rom. 5:6
* Rom. 5:12
* Rom. 6:11
* Rom. 6:23
* Rom. 8:2
* Rom. 8:9
* Rom. 8:10
* Rom. 8:14
* Rom. 8:16
* Rom. 8:17
* Rom. 8:26
* Rom. 8:27
* Rom. 8:28
* Rom. 8:29
* Rom. 8:32
* Rom. 9:5
* Rom. 10:1
* Rom. 10:12
* Rom. 11:33
* Rom. 12:1
* Rom. 12:19
* Rom. 14:10
* Rom. 14:11
* Rom. 15:13
* Rom. 15:16
* Rom. 15:19
* Rom. 15:30
* Rom. 16:25
* Rom. 16:26
* 1 Cor. 1:2
* 1 Cor. 2:3
* 1 Cor. 2:10
* 1 Cor. 2:11
* 1 Cor. 2:12
* 1 Cor. 2:13
* 1 Cor. 2:14
* 1 Cor. 3:16
* 1 Cor. 3:17
* 1 Cor. 3:23
* 1 Cor. 6:9
* 1 Cor. 6:19
* 1 Cor. 8:6
* 1 Cor. 9:22
* 1 Cor. 11:3
* 1 Cor. 12:3
* 1 Cor. 12:11
* 1 Cor. 13:12
* 1 Cor. 14:25
* 1 Cor. 15:10
* 1 Cor. 15:24
* 1 Cor. 15:28
* 1 Cor. 16:22
* 2 Cor. 1:3
* 2 Cor. 1:4
* 2 Cor. 2:16
* 2 Cor. 3:5
* 2 Cor. 3:17
* 2 Cor. 3:18
* 2 Cor. 4:6
* 2 Cor. 5:10
* 2 Cor. 5:14
* 2 Cor. 5:15
* 2 Cor. 5:19
* 2 Cor. 5:20
* 2 Cor. 5:21
* 2 Cor. 6:16
* 2 Cor. 8:9
* 2 Cor. 10:5
* 2 Cor. 11:2
* 2 Cor. 13:14
* Gal. 1:1
* Gal. 1:3
* Gal. 1:4
* Gal. 1:5
* Gal. 3:13
* Gal. 4:6
* Gal. 6:2
* Gal. 6:8
* Eph. 1:1
* Eph. 1:2
* Eph. 1:3
* Eph. 1:4
* Eph. 1:5
* Eph. 1:6
* Eph. 1:7
* Eph. 1:11
* Eph. 1:13
* Eph. 1:14
* Eph. 1:17
* Eph. 1:18
* Eph. 1:19
* Eph. 1:20
* Eph. 1:21
* Eph. 1:22
* Eph. 1:23
* Eph. 2:4
* Eph. 2:5
* Eph. 2:18
* Eph. 3:8
* Eph. 3:9
* Eph. 3:16
* Eph. 3:17
* Eph. 3:19
* Eph. 4:4
* Eph. 4:5
* Eph. 4:6
* Eph. 4:8
* Eph. 4:10
* Eph. 4:30
* Eph. 4:32
* Eph. 5:5
* Eph. 5:25
* Eph. 6:17
* Phil. 1:1
* Phil. 2:1
* Phil. 2:6
* Phil. 2:7
* Phil. 2:9
* Phil. 2:10
* Phil. 2:11
* Phil. 3:18
* Phil. 3:19
* Phil. 4:13
* Col. 1:8
* Col. 1:13
* Col. 1:15
* Col. 1:16
* Col. 1:17
* Col. 1:18
* Col. 1:19
* Col. 1:20
* Col. 1:27
* Col. 2:3
* Col. 2:9
* Col. 2:15
* Col. 3:11
* Col. 3:13
* Col. 3:16
* Col. 3:17
* Col. 3:24
* 1 Thess. 1:1
* 1 Thess. 1:10
* 1 Thess. 3:11
* 1 Thess. 3:12
* 1 Thess. 3:13
* 2 Thess. 1:1
* 2 Thess. 1:7
* 2 Thess. 1:8
* 2 Thess. 1:9
* 2 Thess. 1:12
* 2 Thess. 2:12
* 2 Thess. 2:13
* 2 Thess. 2:14
* 2 Thess. 2:16
* 2 Thess. 2:17
* 2 Thess. 3:5
* 1 Tim. 1:1
* 1 Tim. 1:15
* 1 Tim. 1:17
* 1 Tim. 2:5
* 1 Tim. 2:6
* 1 Tim. 4:1
* 1 Tim. 6:15
* 1 Tim. 6:16
* 2 Tim. 1:10
* 2 Tim. 2:1
* 2 Tim. 2:22
* 2 Tim. 3:16
* Titus 1:1
* Titus 1:3
* Titus 1:4
* Titus 2:10
* Titus 2:11
* Titus 2:12
* Titus 2:13
* Titus 2:14
* Titus 3:4
* Titus 3:5
* Titus 3:6
* Heb. 1:1
* Heb. 1:2
* Heb. 1:3
* Heb. 1:4
* Heb. 1:5
* Heb. 1:6
* Heb. 1:7
* Heb. 1:8
* Heb. 1:9
* Heb. 1:10
* Heb. 1:11
* Heb. 1:12
* Heb. 1:13
* Heb. 2:3
* Heb. 2:10
* Heb. 2:11
* Heb. 2:13
* Heb. 2:14
* Heb. 2:15
* Heb. 2:17
* Heb. 2:18
* Heb. 3:7
* Heb. 3:8
* Heb. 3:9
* Heb. 4:13
* Heb. 4:15
* Heb. 5:7
* Heb. 5:9
* Heb. 6:17
* Heb. 6:18
* Heb. 7:25
* Heb. 7:26
* Heb. 9:14
* Heb. 10:4
* Heb. 10:27
* Heb. 10:28
* Heb. 10:29
* Heb. 10:30
* Heb. 10:31
* Heb. 11:6
* Heb. 11:16
* Heb. 11:27
* Heb. 12:21
* Heb. 12:29
* Heb. 13:8
* Heb. 13:20
* James 1:1
* James 1:15
* James 2:5
* James 2:10
* James 4:12
* James 5:20
* 1 Pet. 1:1
* 1 Pet. 1:2
* 1 Pet. 1:7
* 1 Pet. 1:8
* 1 Pet. 1:9
* 1 Pet. 1:17
* 1 Pet. 1:21
* 1 Pet. 2:7
* 1 Pet. 2:8
* 1 Pet. 2:25
* 1 Pet. 3:18
* 1 Pet. 3:20
* 1 Pet. 3:21
* 1 Pet. 3:22
* 1 Pet. 4:17
* 1 Pet. 4:18
* 1 Pet. 4:19
* 1 Pet. 5:2
* 1 Pet. 5:4
* 1 Pet. 5:11
* 2 Pet. 1:1
* 2 Pet. 1:2
* 2 Pet. 1:4
* 2 Pet. 1:11
* 2 Pet. 1:21
* 2 Pet. 2:17
* 2 Pet. 3:18
* 1 John 1:2
* 1 John 1:3
* 1 John 1:5
* 1 John 1:7
* 1 John 2:1
* 1 John 2:2
* 1 John 2:15
* 1 John 2:23
* 1 John 3:5
* 1 John 4:8
* 1 John 5:1
* 1 John 5:3
* 1 John 5:4
* 1 John 5:6
* 1 John 5:10
* 1 John 5:19
* 1 John 5:20
* 2 John 9
* Jude 1
* Jude 14
* Jude 15
* Jude 20
* Jude 21
* Rev. 1:4
* Rev. 1:5
* Rev. 1:6
* Rev. 1:8
* Rev. 1:10
* Rev. 1:11
* Rev. 1:17
* Rev. 1:18
* Rev. 2:10
* Rev. 2:18
* Rev. 2:19
* Rev. 2:20
* Rev. 2:21
* Rev. 2:22
* Rev. 2:23
* Rev. 3:1
* Rev. 3:9
* Rev. 3:14
* Rev. 3:19
* Rev. 3:21
* Rev. 4:5
* Rev. 4:6
* Rev. 4:8
* Rev. 4:10
* Rev. 4:11
* Rev. 5:6
* Rev. 5:8
* Rev. 5:9
* Rev. 5:10
* Rev. 5:11
* Rev. 5:12
* Rev. 5:13
* Rev. 5:14
* Rev. 6:16
* Rev. 6:17
* Rev. 7:10
* Rev. 7:11
* Rev. 7:17
* Rev. 11:15
* Rev. 11:16
* Rev. 14:4
* Rev. 14:7
* Rev. 14:13
* Rev. 15:3
* Rev. 15:4
* Rev. 19:3
* Rev. 19:4
* Rev. 19:6
* Rev. 19:10
* Rev. 19:13
* Rev. 19:15
* Rev. 19:16
* Rev. 20:14
* Rev. 20:15
* Rev. 21:8
* Rev. 21:9
* Rev. 21:22
* Rev. 21:23
* Rev. 22:1
* Rev. 22:2
* Rev. 22:3
* Rev. 22:9
* Rev. 22:11
* Rev. 22:12
* Rev. 22:17
