# Tools

This folder contains various tools to assist in the writing of the manuscript.

## Spelling dictionary tools

A custom spelling dictionary for the manuscript is included in the repository.

### Updating LibreOffice spelling dictionary from repository

The spelling dictionary needs to be copied to your LibreOffice installation when first editing the manuscript or after pulling the latest changes from the remote repository in which the spelling dictionary has been updated. In either of these scenarios, you need to copy the changed file from the local repository so it will be used the next time you edit the manuscript. Run the following command to update your LibreOffice spelling dictionary with the latest copy in the local repository:

```sh
$ ./tools/copy_spelling_dictionary_to_lo
```

Restart LibreOffice (not just Writer!) for the updated spelling dictionary to be recognized.

In addition, if this is the first time you are editing the manuscript, you need to enable the spelling dictionary within LibreOffice. From LibreOffice Writer, perform the following steps:

1. From the main menu, select **Tools > Spelling...**.
1. Click **Options...**.
1. Set the check box for the entry **The Trinity**.
    1. Click **Edit...**.
    1. Change the **Language** field to **[All]**.
    1. Click **Close**.
1. Unset the check box for all other entries.
1. Click **OK**.
1. Click **Close**.

### Updating repository spelling dictionary from LibreOffice

If during the course of editing the manuscript, you update your LibreOffice spelling dictionary, you need to copy the changed file to the local repository so it will be included in the commit containing your manuscript changes. Run the following command to update your local repository spelling dictionary with the latest copy in LibreOffice:

```sh
$ ./tools/copy_spelling_dictionary_from_lo
```
