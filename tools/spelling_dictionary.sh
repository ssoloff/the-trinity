#!/usr/bin/env bash
#
# Common definitions for working with the manuscript spelling dictionary.
#

# Path to LibreOffice spelling dictionary.
readonly LO_DICTIONARY_FILE=~/.config/libreoffice/4/user/wordbook/The\ Trinity.dic

# Path to repository spelling dictionary (relative to repository root).
readonly REPO_DICTIONARY_FILE=manuscript.dic

# Copies the manuscript spelling dictionary from the current user's LibreOffice configuration to this repository.
function copy_spelling_dictionary_from_lo() {
  cp "${LO_DICTIONARY_FILE}" "${REPO_DICTIONARY_FILE}"
}

# Copies the manuscript spelling dictionary from this repository to the current user's LibreOffice configuration.
function copy_spelling_dictionary_to_lo() {
  cp "${REPO_DICTIONARY_FILE}" "${LO_DICTIONARY_FILE}"
}
