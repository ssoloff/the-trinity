# References

## Ancient Greek

* Ancient Greek basic characters: https://en.wikipedia.org/wiki/Greek_and_Coptic
* Ancient Greek diacritic characters: https://en.wikipedia.org/wiki/Greek_diacritics
* Greek-English Dictionary: https://lsj.gr/wiki/Main_Page
* Unicode quotation marks: https://www.cl.cam.ac.uk/~mgk25/ucs/quotes.html; see also https://corp.unicode.org/pipermail/unicode/2019-January/007565.html for correct character to use in ancient Greek elision

## Bible citations

* Citing the Bible: https://hbl.gcc.libguides.com/c.php?g=339562&p=2285985
* Permission to quote New King James Bible: https://www.thomasnelson.com/about-us/permissions/

## Bibles

* 1611 King James Version with margin notes: http://blackletterkingjamesbible.com/

## Bickersteth

* General search: https://duckduckgo.com/?q=edward+henry+bickersteth+the+trinity+free+pdf&t=brave&ia=web
* The Trinity digital manuscript:
  * http://www.ntslibrary.com/PDF%20Books/The%20Trinity%20-%20E%20H%20Bickersteth.pdf
  * https://rediscoveringthebible.com/Bickersteth.html (best reproduction; includes Greek)

## Forms of address

* The Worshipful the Mayor
  * https://www.west-norfolk.gov.uk/info/20159/mayor_and_deputy_mayor/321/how_to_address_the_mayor (https://archive.md/qq3JW)

## Hebrew

* Transliteration tool
  * https://alittlehebrew.com/transliterate/

## Word usage history

* Google Books Ngram Viewer: https://books.google.com/ngrams
